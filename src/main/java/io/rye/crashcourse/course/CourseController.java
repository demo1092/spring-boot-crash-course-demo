package io.rye.crashcourse.course;

import io.rye.crashcourse.course.data.CourseAddDTO;
import io.rye.crashcourse.course.data.CourseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@RestController
@RequestMapping(path = "/api/v1/courses", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
@AllArgsConstructor
public class CourseController {

  private final CourseService courseService;

  @PostMapping(consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
  public ResponseEntity<CourseDTO> addCourse(@Valid @RequestBody CourseAddDTO request) {
    CourseDTO response = courseService.addCourse(request);
    return new ResponseEntity<>(response, CREATED);
  }

  @GetMapping
  public ResponseEntity<List<CourseDTO>> getAllCourses() {
    List<CourseDTO> courseList = courseService.getAllCourses();
    return new ResponseEntity<>(courseList, OK);
  }

  @PutMapping(path = "{id}")
  public ResponseEntity<CourseDTO> updateCourse(@PathVariable("id") Long id,
                                                @Valid @RequestBody CourseAddDTO request) {
    CourseDTO response = courseService.updateCourse(id, request);
    return new ResponseEntity<>(response, OK);
  }

  @DeleteMapping(path = "{id}")
  public ResponseEntity<?> deleteCourse(@PathVariable("id") Long id) {
    courseService.deleteCourseById(id);
    return new ResponseEntity<>(OK);
  }

}

package io.rye.crashcourse.course;

import io.rye.crashcourse.course.data.CourseAddDTO;
import io.rye.crashcourse.course.data.CourseDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Service
public interface CourseService {

  CourseDTO addCourse(CourseAddDTO request);

  List<CourseDTO> getAllCourses();

  CourseDTO updateCourse(Long id, CourseAddDTO request);

  void deleteCourseById(Long id);

}

package io.rye.crashcourse.course.converter;

import io.rye.crashcourse.course.Course;
import io.rye.crashcourse.course.data.CourseAddDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
public class CourseRequestToEntityConverter implements Converter<CourseAddDTO, Course> {

  @Override
  public Course convert(CourseAddDTO src) {
    return Course.builder()
      .name(src.getName())
      .department(src.getDepartment())
      .createdAt(LocalDateTime.now(ZoneId.of("Asia/Manila")))
      .build();
  }

}

package io.rye.crashcourse.course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Repository
@Transactional(readOnly = true)
public interface CourseRepository extends JpaRepository<Course, Long> {

  @Query("SELECT c FROM Course c ORDER BY c.createdAt DESC")
  List<Course> findAllByOrderByCreatedAtDesc();

  @Query("SELECT c FROM Course c WHERE c.id = :id")
  Optional<Course> findById(@Param("id") Long id);

}

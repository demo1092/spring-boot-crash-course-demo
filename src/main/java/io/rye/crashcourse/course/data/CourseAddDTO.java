package io.rye.crashcourse.course.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseAddDTO {

  @NotNull(message = "Course cannot be empty")
  @Size(min = 2, max = 100, message = "Course length must be 2 to 100")
  private String name;

  @NotNull(message = "Department cannot be empty")
  @Size(min = 2, max = 100, message = "Department length must be 2 to 100")
  private String department;

}

package io.rye.crashcourse.student.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Created on 2022-04-29
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentAddDTO {

  @NotNull(message = "First name cannot be empty")
  @Size(min = 2, max = 100, message = "First name length must be 2 to 100")
  private String firstName;

  @NotNull(message = "Last name cannot be empty")
  @Size(min = 2, max = 100, message = "Last name length must be 2 to 100")
  private String lastName;

  @NotNull(message = "Birthdate cannot be empty")
  @DateTimeFormat(pattern = "YYYY-MM-DD")
  private LocalDate birthdate;

  @NotNull(message = "Email cannot be empty")
  private String personalEmail;

}

package io.rye.crashcourse.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created on 2022-04-29
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Repository
@Transactional(readOnly = true)
public interface StudentRepository extends JpaRepository<Student, Long> {

  @Query("SELECT s FROM Student s ORDER BY s.createdAt DESC")
  List<Student> findAllByOrderByCreatedAtDesc();

  @Query("SELECT s FROM Student s WHERE s.id = :id")
  Optional<Student> findById(@Param("id") Long id);

}

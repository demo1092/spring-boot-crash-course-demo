package io.rye.crashcourse.student;

import io.rye.crashcourse.enrollment.Enrollment;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created on 2022-04-29
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
@Entity(name = "Student")
@Table(
  name = "student",
  uniqueConstraints = {
    @UniqueConstraint(name = "student_email_unique", columnNames = "personal_email")
  }
)
public class Student {

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @SequenceGenerator(
    name = "student_id_sequence",
    sequenceName = "student_id_sequence",
    allocationSize = 1
  )
  @GeneratedValue(strategy = SEQUENCE, generator = "student_id_sequence")
  private Long id;

  @Column(name = "first_name", nullable = false)
  private String firstName;

  @Column(name = "last_name", nullable = false)
  private String lastName;

  @Column(name = "birthdate", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDate birthdate;

  @Column(name = "personal_email", nullable = false)
  private String personalEmail;

  @OneToMany(
    cascade = ALL,
    mappedBy = "student",
    orphanRemoval = true,
    fetch = LAZY
  )
  @ToString.Exclude
  private List<Enrollment> enrollments = new ArrayList<>();

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  @Column(name = "updated_at", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime updatedAt;

  public void addEnrollment(Enrollment enrollment) {
    if (!enrollments.contains(enrollment)) {
      enrollments.add(enrollment);
    }
  }

  public void removeEnrollment(Enrollment enrollment) {
    enrollments.remove(enrollment);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Student student = (Student) o;
    return id != null && Objects.equals(id, student.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

}

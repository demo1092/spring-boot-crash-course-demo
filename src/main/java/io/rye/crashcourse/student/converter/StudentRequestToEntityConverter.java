package io.rye.crashcourse.student.converter;

import io.rye.crashcourse.student.Student;
import io.rye.crashcourse.student.data.StudentAddDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
@AllArgsConstructor
public class StudentRequestToEntityConverter implements Converter<StudentAddDTO, Student> {

  @Override
  public Student convert(StudentAddDTO src) {
    return Student.builder()
      .firstName(src.getFirstName())
      .lastName(src.getLastName())
      .birthdate(src.getBirthdate())
      .personalEmail(src.getPersonalEmail())
      .createdAt(LocalDateTime.now(ZoneId.of("Asia/Manila")))
      .build();
  }

}

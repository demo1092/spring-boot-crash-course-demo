package io.rye.crashcourse.student;

import io.rye.crashcourse.enrollment.data.EnrollmentAddDTO;
import io.rye.crashcourse.student.data.StudentAddDTO;
import io.rye.crashcourse.student.data.StudentDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on 2022-04-29
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Service
public interface StudentService {

  StudentDTO addStudent(StudentAddDTO request);

  List<StudentDTO> getAllStudents();

  StudentDTO updateStudent(Long id, StudentAddDTO request);

  void deleteStudentById(Long id);

  String addStudentEnrollment(Long id, EnrollmentAddDTO request);

}

package io.rye.crashcourse.enrollment.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnrollmentAddDTO {

  @NotNull(message = "Course cannot be empty")
  private Long courseId;

}

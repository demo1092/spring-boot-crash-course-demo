package io.rye.crashcourse.enrollment;

import io.rye.crashcourse.course.Course;
import io.rye.crashcourse.student.Student;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Enrollment")
@Table(name = "enrollment")
public class Enrollment {

  @EmbeddedId
  private EnrollmentId id;

  @ManyToOne
  @MapsId("studentId")
  @JoinColumn(
    name = "student_id",
    foreignKey = @ForeignKey(name = "enrollment_student_id_fk")
  )
  private Student student;

  @ManyToOne
  @MapsId("courseId")
  @JoinColumn(
    name = "course_id",
    foreignKey = @ForeignKey(name = "enrollment_course_id_fk")
  )
  private Course course;

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Enrollment that = (Enrollment) o;
    return id != null && Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

}

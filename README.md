# Spring Boot Crash Course

## Requirements
Install the following software applications.
- Java 8
- Postman
- Docker
- Intellij IDEA
- DBMS such as HeidiSQL, DBeaver, etc.

## Resources
- Architecture and database schema [link](https://app.diagrams.net/#Wa5beee6f7db3dd5a%2FA5BEEE6F7DB3DD5A!29744).
- Spring Data JPA Query Methods [documentation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods).

## Getting Started
1. Execute the following command to create the PostgreSQL database: `docker-compose up -d`
2. Connect to the database using your DBMS and create a database named: `crashcourse`
3. Open the project using Intellij IDEA to download the dependencies.

## Author
Hi, I'm [Ryan](https://www.linkedin.com/in/ryan-jan-borja-a0997844/).